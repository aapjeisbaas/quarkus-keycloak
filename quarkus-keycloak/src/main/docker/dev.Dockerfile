FROM quay.io/quarkus/centos-quarkus-maven:19.2.1

WORKDIR /code

COPY . .

EXPOSE 8080

CMD ["./mvnw", "clean", "compile", "quarkus:dev"]